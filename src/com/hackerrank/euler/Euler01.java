package com.hackerrank.euler;

import java.util.Scanner;

public class Euler01 {
	//Scored 60!
	public static void main(String[] args) {
		
	    Scanner in = new Scanner(System.in);
        int count = in.nextInt();
     
        for(int a=0;a<count;a++){
        	int t= in.nextInt();
        	   int result = 0;
        	   for(int b=0;b<t;b++){
        		   if((b%3 ==0)|| (b%5 ==0)){
        			   result +=b;
        		   }
        	   }
        	   System.out.println(result);
        }
		
	}
}
