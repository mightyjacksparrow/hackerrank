package com.hackerrank.euler;

import java.util.Scanner;

public class Euler02 {
	public static void main(String[] args) {
		  Scanner in = new Scanner(System.in);
	        int count = in.nextInt();
	      
	        for(int a=0;a<count;a++){
	        	int n= in.nextInt();
	        	int f0 = 2;
	        	int f1 = 8;
	        	int f2 = 0;
	        	 int  result =0 ;
	        	while(f1<=n){
	        		result +=f1;
	        		 f2 = 4*f1 + f0;
	        	      f0=f1;
	        	      f1=f2;
	        	}
	        	 System.out.println(result+2);
	        }
	}
}
