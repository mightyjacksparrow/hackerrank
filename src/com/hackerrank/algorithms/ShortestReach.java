package com.hackerrank.algorithms;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

//https://www.hackerrank.com/tesla1986?hr_r=1
public class ShortestReach {
	
	
	//Define Node
	public static class Node{
			int ID;
			int distance;
			ArrayList<Integer> neighbours;
			
			public Node(int i){
				ID= i;
				distance = -1;
				neighbours = new ArrayList<Integer>();
			}
			
			public void addNeighbour(int i){
				neighbours.add(i);
			}
	}
	
	//main Method
	public static void main(String[] args) {
	      Scanner input = new Scanner(System.in);
	        int q = input.nextInt();
	        for(int query=0; query<q; query++){
	        	 int n = input.nextInt();
	        	 int m = input.nextInt();
	        	 
	        	 ArrayList<Node> graph  = new ArrayList<>();
	        	 
			        for(int i=0;i<=n;i++){
			        	graph.add(new Node(i));
			        }
		        
	        for(int i=0;i<m;i++){
	        	int u = input.nextInt();
	        	int v = input.nextInt();
	        	
	        	graph.get(u).addNeighbour(v); //undirected Graph
	        	graph.get(v).addNeighbour(u);
	        }
	        
	        int s = input.nextInt(); //starting node
            bfs(graph, s);
            System.out.println();
	        	
	        	
	        }

	}
	
	
	
	//BFS
	public static void bfs(ArrayList<Node> graph , int start){
		
		ArrayList<Integer> visited  = new ArrayList<Integer>();
		Queue<Integer> q = new LinkedList<>();
		q.add(start);
		
		int current;
		
		while (!q.isEmpty()){
			current = q.remove();
			Node n = graph.get(current);
			
			for(Integer i: n.neighbours){
				if(!visited.contains(i)){
					q.add(i);
					visited.add(i);
					  if(n.distance == -1){
							graph.get(i).distance = 6;
					  }
				else{
					graph.get(i).distance = n.distance+6;
				}
				}
			}
		}
		
		for(Node n: graph){
			if(n.ID!= start && n.ID!=0){
				 System.out.print(n.distance + " ");
			}
		}
		
		
	}

	
}
