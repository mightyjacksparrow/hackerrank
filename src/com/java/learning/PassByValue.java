package com.java.learning;

import java.util.ArrayList;
import java.util.List;

public class PassByValue {

	//https://www.youtube.com/watch?v=_y7k_0edvuY
	//http://www.journaldev.com/3884/java-is-pass-by-value-and-not-pass-by-reference
	public static void main(String[] args) {
		
		List<String> stringList = new ArrayList<String>();
		stringList.add("A");
		stringList.add("B");
		
		PassByValue obj = new PassByValue();
	    obj.passlList(stringList);
		//Test Commit
		System.out.println(stringList.size());
		System.out.println(stringList.hashCode());
		
	}
	
	private  void passlList(List<String> input){
		System.out.println("_________" + input.hashCode());
		input.add("Hell");
	}

}
