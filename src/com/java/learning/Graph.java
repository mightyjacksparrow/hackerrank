package com.java.learning;

import java.util.Iterator;
import java.util.LinkedList;

public class Graph {
	
	//With LinkedList Implementation
	private int  V;
	
	private LinkedList<Integer> adj[];
	
	Graph(int c){
		V=c;
		adj = new LinkedList[c];
		
		for(int l=0;l<c;++l){
			adj[l] = new LinkedList();
		}
		
	}
	void addEdge(int v, int w)
    {
        adj[v].add(w);  // Add w to v's list.
    }
	
	void DFSUtil(int v, boolean visited[]){
		visited[v] = true;
		System.out.println(v+ " ");
		
		Iterator<Integer> i = adj[v].listIterator();
        while (i.hasNext())
        {
            int n = i.next();
            if (!visited[n])
                DFSUtil(n, visited);
        }
		
	
	}
	
	void DFS(int v)
    {
        // Mark all the vertices as not visited(set as
        // false by default in java)
        boolean visited[] = new boolean[V];
 
        // Call the recursive helper function to print DFS traversal
        DFSUtil(v, visited);
    }
	
	public static void main(String[] args) {
		Graph f = new Graph(10);
		
		f.addEdge(0, 2);
		f.addEdge(1,8);
		f.addEdge(1, 4);
		f.addEdge(2, 6);
		f.addEdge(2, 8);
		
		f.addEdge(3, 5);
		f.addEdge(6, 9);
		
		f.DFS(0);
		System.out.println(("______________"));
		f.DFS(1);
		System.out.println(("______________"));
		f.DFS(2);
		System.out.println(("______________"));
		f.DFS(3);
		System.out.println(("______________"));
		f.DFS(4);
		System.out.println(("______________"));
		f.DFS(5);
		System.out.println(("______________"));
		
		
		
		
	}
	

}
