//$Id$
package com.desk.technical;

import java.awt.print.Book;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CSVReader {

	 public static void main(String... args) {
		 String desktop = System.getProperty ("user.home") ;
		   Path pathToFile =  Paths.get(desktop+"‎⁨/Desktop/dummy.txt");
		   System.out.println(pathToFile.getParent());
		   try (BufferedReader br = Files.newBufferedReader(pathToFile,
	                StandardCharsets.US_ASCII)) {

	            // read the first line from the text file
	            String line = br.readLine();

	            // loop until all lines are read
	            while (line != null) {

	                // use string.split to load a string array with the values from
	                // each line of
	                // the file, using a comma as the delimiter
	                String[] attributes = line.split(",");
	                System.out.println(attributes);

	                // read next line before looping
	                // if end of file reached, line would be null
	                line = br.readLine();
	            }

	        } catch (IOException ioe) {
	            ioe.printStackTrace();
	        }

	    }
	
}
