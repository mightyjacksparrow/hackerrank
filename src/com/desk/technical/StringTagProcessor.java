//$Id$
package com.desk.technical;

import java.util.StringTokenizer;

public class StringTagProcessor {
	
	
	public static void main(String[] args) {
		
		String message = "<img src=\"https://scontent.xx.fbcdn.net/v/t35.0-12/27479563_221176901784236_729515486_o.png?oh=8de26b9c18c2a3bc8c6a9e26ed856c9f&amp;oe=5A6FEC54\" height=\"150px\" width=\"150px\">";
		StringTokenizer strToken=new StringTokenizer(message," ");
		
		while(strToken.hasMoreTokens()){
			String token=strToken.nextToken();
			if(token.startsWith("height")){
			 continue;	
			}
			if(token.startsWith("width")){
				token = ">";
			}
			System.out.println(token);
		}
	}

}
