//$Id$
package com.desk.technical;

import java.net.URLDecoder;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class StringMapParse {
public static void main(String[] args) {
//	Map<String,String> formData = new HashMap<String,String>();
//	formData.put("From", "whatsapp:"+ "1");//+15005550006 dummy from Number to be Updated. Now +14155238886 only works
//	formData.put("Body", "2");
//	formData.put("To","whatsapp:");
//	String s= convertWithStream(formData);
//	System.out.println(s);
//	Map<String,String> ssss = convertWithStream(s);
//	System.out.println(s);
	
	
//	String fromStr=  URLDecoder.decode("whatsapp:+919952610640").split(":")[1];
	String fromStr=  "whatsapp:+919952610640".split(":")[1];
	System.out.println(fromStr);
}
public static String convertWithStream(Map<?, ?> map) {
	 String mapAsString = map.keySet().stream()
		      .map(key -> key + "=" + map.get(key))
		      .collect(Collectors.joining(", "));
		    return mapAsString;
}

public static Map<String, String> convertWithStream(String mapAsString) {
	 Map<String, String> map = Arrays.stream(mapAsString.split(", "))
		      .map(entry -> entry.split("="))
		      .collect(Collectors.toMap(entry -> entry[0], entry -> entry[1]));
		    return map;
}
}
