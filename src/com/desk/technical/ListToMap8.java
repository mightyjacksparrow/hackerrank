//$Id$
package com.desk.technical;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ListToMap8 {
	 public static void main(String[] args) {
			
		 class abc{
			 public String sss;
			 
			 public String getSss(){
				 return sss;
			 }
			 
			 public void setSss(String sss){
				 this.sss = sss;
			 }
			 
			 public abc(String sss){
				 this.sss = sss;
			 }
		 }
		 
		 List<abc> ooo = new ArrayList<abc>();
		 ooo.add(new abc("1"));
		 ooo.add(new abc("2"));
		 ooo.add(new abc("3"));
		 
		 Map<String,abc> wwww = ooo.stream().collect(
	                Collectors.toMap(abc::getSss,Function.identity()));
		 
		 System.out.println(wwww.keySet());
	}

}
