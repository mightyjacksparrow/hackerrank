//$Id$
package com.desk.technical;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class NullPointerCheck {
	public static final String ISO_DATETIME_PATTERN1 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"; // NO I18N
	private static DateTimeFormatter formatter = new DateTimeFormatterBuilder().append(DateTimeFormatter.ofPattern(ISO_DATETIME_PATTERN1)).toFormatter();

	public static void main(String[] args) {
//		String s = null;
//		try {
//			System.out.println(s.toLowerCase());
//		} catch (Exception e) {
//			System.out.println("Exception caught, and done nothing");
//		}
//		
//		Timestamp t = isoStringToTimestamp("2020-06-16T11:17:50.000Z");
//		int sessionResetTimeInMillis = 10 * 60 * 1000;
//		boolean newMessageAfterResetTime = true ? (t.getTime() + sessionResetTimeInMillis 
//				<= DateTimeUtils.getCurrentTimeInGMT().getTime()) : false;
//
//		System.out.println(newMessageAfterResetTime);
//		System.out.println(t.getTime());
//		
//		System.out.println(DateTimeUtils.getCurrentTimeInGMT().getTime());

	}

	public static Timestamp isoStringToTimestamp(String dateTimeString) {
		LocalDateTime dateTime = LocalDateTime.from(formatter.parse(dateTimeString));
		return Timestamp.valueOf(dateTime);
	}

}
