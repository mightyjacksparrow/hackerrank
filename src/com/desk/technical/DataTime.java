//$Id$
package com.desk.technical;

import java.time.Instant;
import java.time.OffsetDateTime;

public class DataTime {
	public static void main(String[] args) {
		//2021-03-25 04:34:51+00:00
		String input = "2021-03-25 05:09:20+00:00".replace( " " , "T" );
		OffsetDateTime odt = OffsetDateTime.parse( input );
		Instant instant = odt.toInstant();  // Instant is always in UTC.
		java.util.Date date = java.util.Date.from( instant );
//		Date date = new Date();
		System.out.println(date.toString());
		System.out.println(date.getTime());
	}

}
