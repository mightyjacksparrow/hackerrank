//$Id$
package com.desk.technical;

public class ExceptionTest {
	
	public static void main(String[] args) {
		
		String s= null;
				
		try{
//			System.out.println(s.charAt(9));
			System.out.println("a");
			System.out.println(Boolean.TRUE);
		}catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println();
			
			System.out.println(e.getStackTrace()[0]);
			System.out.println();
			
			System.out.println(e);
			System.out.println();
			
			System.out.println(e.getLocalizedMessage());
			System.out.println();
			
			System.out.println(e.getStackTrace().toString());
		}
		
		
		
		
	}

}
