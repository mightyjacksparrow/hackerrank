//$Id$
package com.codewars.java.best;

public class WhoLikesIt {
	public static String whoLikesIt(String... names) {
		final String LIKETHIS = " like this";
		final String LIKESTHIS = " likes this";
		StringBuilder preString = new StringBuilder();
        //Do your magic here
      if(names.length==0) {
    	  preString.append("no one").append(LIKESTHIS);
      }else if(names.length==1) {
    	  preString.append(names[0]).append(LIKESTHIS);
      }
      else if(names.length==2) {
    	  preString.append(names[0]).append(" and ").append(names[1]).append(LIKETHIS);
      }
      else if(names.length==3) {
    	  preString.append(names[0]).append(", ").append(names[1]).append(" and ").append(names[2]).append(LIKETHIS);
      }else {
    	  preString.append(names[0]).append(", ").append(names[1]).append(" and ").append(names.length-2 + " others").append(LIKETHIS);
      }
      return preString.toString();
    }
public static void main(String[] args) {
	System.out.println(whoLikesIt (new String[]{})); // must be "no one likes this"
	System.out.println(whoLikesIt (new String[]{"Peter"})); // must be "Peter likes this"
	System.out.println(whoLikesIt (new String[]{"Jacob", "Alex"}));// must be "Jacob and Alex like this"
	System.out.println(whoLikesIt (new String[]{"Max", "John", "Mark"})); // must be "Max, John and Mark like this"
	System.out.println(whoLikesIt (new String[]{"Alex", "Jacob", "Mark", "Max"})); 
}
}
