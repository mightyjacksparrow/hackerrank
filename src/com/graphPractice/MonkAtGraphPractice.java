package com.graphPractice;

import java.util.Scanner;

//
//for any tree, number of edges ( E ) = number of vertices (V) - 1
//and each edge contribute 2 degree.. hence total degree will be (E) * 2 = 2 * (n-1)..

//you should know that a graph has a total sum of all degree of vertices that is atleast 2 * E, where E is the number of edges. 

public class MonkAtGraphPractice {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int e = s.nextInt();
		int sum =0;
		for(int a =0;a<e;a++){
			sum += s.nextInt();
		}
		if(e==sum-1){
			System.out.println("YES"); 
		}else{
			System.out.println("NO");
		}
	}

}
